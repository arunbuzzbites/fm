import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import normalize from 'react-native-normalize';
import App from './App';
import {Color} from './constants/Color';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';

const Stack = createStackNavigator();

export default class HomeScreenNavigator extends React.PureComponent {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: Color.BACKGROUND_COLOR,
          },
          //  headerTintColor: 'white',
          headerTitleStyle: {fontWeight: 'bold'},
          headerLeft: () => (
            <View style={styles.container}>
              <Image source={require('./img/logo2.png')} />
            </View>
          ),
          headerRight: () => (
            <View style={styles.container}>
              <TouchableOpacity style={styles.buttonStyle}>
                <Image source={require('./img/translate-1.png')} />
              </TouchableOpacity>
            </View>
          ),
        }}>
        <Stack.Screen name="Home" component={App} options={{title: ''}} />
      </Stack.Navigator>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    //  flex: 1,
    marginStart: normalize(5),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    padding: normalize(5),
    fontSize: normalize(16),
    color: 'white',
  },
  buttonStyle: {
    padding: normalize(5),
  },
  headerStyle: {
    marginStart: normalize(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
