import localStorage from 'redux-persist/es/storage';

const apiEntry = 'https://aimlyticsjson-bcrm.magnaquest.com/RestApi';
const usersNamespace = 'users';
const createUser = 'AddOperationalEntity';
const generateOtp = 'GENERATEOTP';
const validateOtp = 'VALIDATEOTP';
const authenticateUser = 'AuthenticateUser';
const dashboard = 'GetRecordsBySearch';
import AsyncStorage from '@react-native-community/async-storage';
import {LocalStore} from '../store/LocalStore';
export default class JSONPlacholderAPI {
  static apiEntry = apiEntry;
  static usersNamespace = usersNamespace;

  // static async fetchUser(user: any) {
  //   const url = `${apiEntry}/${usersNamespace}/${userID}`;
  //    return fetch(url).then((response) => response.json());
  // }
  static async fetchDashboard(user: any) {
    const url = `${apiEntry}/${dashboard}?ReferenceNo=${dashboard}-${Date.now()}`;
    console.debug(url);

    return fetch(url, {
      method: 'Post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        USERNAME: 'BUZZBYTES',
        PASSWORD: 'BUZZBYTES123',
        EXTERNALPARTY: 'MQS',
      },
      body: JSON.stringify(user),
    }).then((response) => response.json());
  }

  static async fetchUser(user: any) {
    const url = `${apiEntry}/${authenticateUser}?ReferenceNo=${authenticateUser}-${Date.now()}`;
    console.debug(url);

    //  return (dispatch) => {
    return fetch(url, {
      method: 'Post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        USERNAME: 'BUZZBYTES',
        PASSWORD: 'BUZZBYTES123',
        EXTERNALPARTY: 'MQS',
      },
      body: JSON.stringify(user),
    }).then((response) => response.json());
    // .then((json: Response) => LocalStore.save(json));
    // };
  }

  static async createUser(user: any) {
    const url = `${apiEntry}/${createUser}?ReferenceNo=${createUser}-${Date.now()}`;
    return fetch(url, {
      method: 'Post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        USERNAME: 'BUZZBYTES',
        PASSWORD: 'BUZZBYTES123',
        EXTERNALPARTY: 'MQS',
      },
      body: JSON.stringify(user),
    }).then((response) => response.json());
  }

  static async generateOTP(genOtp: any) {
    const url = `${apiEntry}/${generateOtp}?ReferenceNo=${generateOtp}-${Date.now()}`;
    console.debug(url);
    return fetch(url, {
      method: 'Post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        USERNAME: 'BUZZBYTES',
        PASSWORD: 'BUZZBYTES123',
        EXTERNALPARTY: 'MQS',
      },
      body: JSON.stringify(genOtp),
    }).then((response) => response.json());
  }

  static async validateOtp(genOtp: any) {
    const url = `${apiEntry}/${validateOtp}?ReferenceNo=${validateOtp}-${Date.now()}`;
    return fetch(url, {
      method: 'Post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        USERNAME: 'BUZZBYTES',
        PASSWORD: 'BUZZBYTES123',
        EXTERNALPARTY: 'MQS',
      },
      body: JSON.stringify(genOtp),
    }).then((response) => response.json());
  }
}
