import {NavigationContainerRef} from '@react-navigation/native';
import * as React from 'react';
export const navigationRef: React.RefObject<NavigationContainerRef> = React.createRef();

export default class NavigationService {
  static navigate(name: string, params?: object) {
    navigationRef.current?.navigate(name, params);
  }

  static resetAndNavigate(name: string, params?: object) {
    navigationRef.current?.reset({
      index: 0,
      routes: [{name: name , params: params}],
    });
  }
  static goBack() {
    navigationRef.current?.goBack();
  }
}
