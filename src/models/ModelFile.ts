export const OPERATIONALENTITYINFO = {
  ENTITYCODE: '',
  ENTITYTYPE: '',
  ENTITYNAME: '',
  BOOKORDERS: 'Y',
  RECEIVEPAYMENTS: 'Y',
  HOLDINVENTORY: 'Y',
  RESELLHARDWARE: '',
  RESPFORCUSTOMERDUES: 'Y',
  STATUS: '',
  NOOFCONCURRENTUSERS: '3',
  PARENTENTITYID: '5078',
  PORTALACCESSREQD: 'Y',
  USERNAME: '',
  PASSWORD: '',
  MOBILEPHONE: '',
  CURRENCYCODE: 'INR',
  NATIONALITY: 'INDIAN',
  UINTYPE: '',
  UIN: '',
  RMNVERIFIED: 'Y',
  ADDRESSINFO: {},
};

export const ADDRESSINFO = {
  ADDRESSTYPECODE: 'PRI',
  COUNTRY: 'INDIA',
  ADDRESS1: '',
  ADDRESS2: '',
  AREA: '',
  CITY: '',
  CITYCODE: '',
  ZIPCODE: '',
  LOCATION: '',
};

export const RegisterUser = {
  OPERATIONALENTITYINFO: {},
};

export const GENERATEOTP = {
  MOBILEPHONE: '',
  OTPEMAIL: '',
  PARTYID: '0',
  COUNTRYCODE: '',
  RESEND: 'FALSE',
  ATTRIBUTE1: '',
};

export const RegisterOtp = {
  GENERATEOTP: {},
};

export const VALIDATEOTP = {
  MOBILEPHONE: '',
  OTPEMAIL: '',
  OTP: '',
};

export const ReValidateOtp = {
  VALIDATEOTP: {},
};

export const AUTHENTICATEUSER = {
  USERNAME: '',
  PASSWORD: '',
};

export const AuthenticateUser = {
  AUTHENTICATEUSER: {},
};
export const dashboard = {
  KEY_NAMEVALUE: {},
  ADDITIONAL_INFO: '',
};
export const KEY_NAMEVALUE = {
  KEY_NAME: '',
  KEY_VALUE: '',
};
export const ADDITIONAL_INFO = {
  DISTRIBUTORID: '',
};
