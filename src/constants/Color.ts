export class Color {
  public static BACKGROUND_COLOR = '#222222';
  public static ICON_COLOR = '#777777';
  public static APP_YELLOW = '#faec4e';
}
