import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import HomeScreenNavigator from './HomeScreenNavigator';
import LoginNavigator from './LoginNavigator';
import OrderHistoryNavigator from './navigators/OrderHistoryNavigator';
import SaleHistoryNavigator from './navigators/SaleHistoryNavigator';
import UpdateProfileNavigator from './navigators/UpdateProfileNavigator';
import {ReviewScreenSaleOne} from './containers/ReviewScreenSaleOne';
import {ReviewScreenSaleTwo} from './containers/ReviewScreenSaleTwo';

const Stack = createStackNavigator();

export default class AppNavigator extends React.PureComponent {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={LoginNavigator} options={{title: 'Register'}} />
        <Stack.Screen name="homeScreen" component={HomeScreenNavigator} options={{title: ''}} />
        <Stack.Screen name="OrderHistory" component={OrderHistoryNavigator} options={{title: ''}} />
        <Stack.Screen name="SaleHistory" component={SaleHistoryNavigator} options={{title: ''}} />
        <Stack.Screen name="ReviewSaleOne" component={ReviewScreenSaleOne} options={{title: ''}} />
        <Stack.Screen name="ReviewSaleTwo" component={ReviewScreenSaleTwo} options={{title: ''}} />
        <Stack.Screen
          name="UpdateProfile"
          component={UpdateProfileNavigator}
          options={{title: ''}}
        />
      </Stack.Navigator>
    );
  }
}
