import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Login from './containers/Login';
import Register from './containers/Register';
import {ResetPassword} from './containers/ResetPassword';
import VerifyOtp from './containers/VerifyOtp';

const Stack = createStackNavigator();

export default class LoginNavigator extends React.PureComponent {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="LoginScreen" component={Login} options={{title: 'Login'}} />
        <Stack.Screen name="Register" component={Register} options={{title: 'Register'}} />
        <Stack.Screen
          name="ResetPassword"
          component={ResetPassword}
          options={{title: 'ResetPassword'}}
        />
        <Stack.Screen name="VerifyOTP" component={VerifyOtp} options={{title: 'VerifyOTP'}} />
      </Stack.Navigator>
    );
  }
}
