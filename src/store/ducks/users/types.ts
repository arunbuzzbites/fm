export enum UsersTypes {
  FETCH = 'USER/FETCH',
  FETCH_SUCCCES = 'USER/FETCH_SUCCCES',
  FETCH_FAILURE = 'USER/FETCH_FAILURE',
}

export enum CreateUserTypes {
  CREATE_USER = 'USER/CREATE',
  CREATE_USER_SUCCESS = 'CREATE/SUCCESS',
  CREATE_USER_FAILURE = 'CREATE/FAILURE',
}

export enum OTPTYPE {
  VALIDATETEOTP = 'OTP/VALIDATE',
  GENERATEOTP = 'OTP/GENERATE',
  GENERATE_OTP_SUCCESS = 'OTP/SUCCESS',

  VALIDATE_OTP_SUCCESS = 'OTP/Validate/SUCCESS',
  GENERATE_OTP_FAILURE = 'OTP/FAILURE',
  VALIDATE_OTP_FAILURE = 'OTP/VALIDATE/FAILURE',
}

export enum DashboardType {
  FETCH = 'DASHBOARD/FETCH',
  FETCH_SUCCCES = 'DASHBOARD/FETCH_SUCCCES',
  FETCH_FAILURE = 'DASHBOARD/FETCH_FAILURE',
}
export interface User {
  id?: string;
}

export interface Userstate {
  readonly data: User | null;
  readonly loading: boolean;
  readonly error: boolean;
}
export interface OTPAction {
  type: string;
  payload: any;
}
export interface CreateAction {
  type: string;
  payload: any;
}
export interface FetchAction {
  type: string;
  payload: any;
}
export interface DashboardAction {
  type: string;
  payload: any;
}
export type AppActions = FetchAction | CreateAction | OTPAction | DashboardAction;
