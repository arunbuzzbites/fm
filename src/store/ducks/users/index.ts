import {UsersTypes, CreateUserTypes, Userstate, FetchAction, AppActions, OTPTYPE, DashboardType} from './types';
import {Reducer} from 'redux';

const initialState: Userstate = {
  data: null,
  loading: false,
  error: false,
};

const reducer: Reducer<Userstate, FetchAction> = (state = initialState, action: AppActions) => {
  console.debug(action.type);
  switch (action.type) {
    case UsersTypes.FETCH_SUCCCES:
      return {...state, data: action.payload};
    case CreateUserTypes.CREATE_USER_SUCCESS:
      return {...state, data: action.payload};
      case OTPTYPE.GENERATE_OTP_SUCCESS:
      return {...state, data: action.payload};
    case OTPTYPE.VALIDATE_OTP_SUCCESS:
      return {...state, data: action.payload};
    case DashboardType.FETCH_SUCCCES:
      return {...state, data: action.payload};
    default:
      return state;
  }
};

export default reducer;
