import {UsersTypes, User, CreateUserTypes, OTPTYPE, DashboardType} from './types';
import {action} from 'typesafe-actions';

export const fetchUser = (payload: any) => action(UsersTypes.FETCH, payload);

export const fetchUserSuccess = (payload: User) => action(UsersTypes.FETCH_SUCCCES, payload);

export const fetchUserFailure = (payload: string) => action(UsersTypes.FETCH_FAILURE, payload);

export const createUser = (payload: any) => action(CreateUserTypes.CREATE_USER, payload);
export const generateOTP = (payload: any) => action(OTPTYPE.GENERATEOTP, payload);
export const validateOtp = (payload: any) => action(OTPTYPE.VALIDATETEOTP, payload);
export const fetchDashboard = (payload: any) => action(DashboardType.FETCH, payload);
export const fetchDashboardSuccess = (payload: any) => action(DashboardType.FETCH_SUCCCES, payload);
export const fetchDashboardFailure = (payload: any) => action(DashboardType.FETCH_FAILURE, payload);


export const generateOtpSuccess = (payload: User) => action(OTPTYPE.GENERATE_OTP_SUCCESS, payload);

export const validateOtpSuccess = (payload: User) => action(OTPTYPE.VALIDATE_OTP_SUCCESS, payload);

export const generateOtpError = (payload: User) => action(OTPTYPE.GENERATE_OTP_FAILURE, payload);

export const validateOtpERROR = (payload: User) => action(OTPTYPE.VALIDATE_OTP_FAILURE, payload);

export const createUserSuccess = (payload: User) =>
  action(CreateUserTypes.CREATE_USER_SUCCESS, payload);

export const createUserFailure = (payload: string) =>
  action(CreateUserTypes.CREATE_USER_FAILURE, payload);
