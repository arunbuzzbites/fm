import {call, put, takeEvery} from 'redux-saga/effects';
import {
  createUserFailure,
  createUserSuccess,
  fetchUserFailure,
  fetchUserSuccess,
  validateOtpSuccess,
  generateOtpSuccess,
  generateOtpError,
  validateOtpERROR, fetchDashboardSuccess, fetchDashboardFailure,
} from './actions';
import {CreateUserTypes, DashboardType, FetchAction, OTPAction, OTPTYPE, UsersTypes} from './types';

import JSONPlacholderAPI from '../../../lib/jsonPlaceholderAPI';

function* fetchUser(action: FetchAction) {
  try {
    const user = yield call(JSONPlacholderAPI.fetchUser, action.payload);
    yield put(fetchUserSuccess(user));
  } catch (e) {
    yield put(fetchUserFailure(e.message));
  }
}
function* fetchDashboard(action: FetchAction) {
  try {
    const user = yield call(JSONPlacholderAPI.fetchDashboard, action.payload);
    yield put(fetchDashboardSuccess(user));
  } catch (e) {
    yield put(fetchDashboardFailure(e.message));
  }
}
function* createUser(action: FetchAction) {
  try {
    const user = yield call(JSONPlacholderAPI.createUser, action.payload);
    yield put(createUserSuccess(user));
  } catch (e) {
    yield put(createUserFailure(e.message));
  }
}
function* GenerateOTP(action: OTPAction) {
  try {
    const user = yield call(JSONPlacholderAPI.generateOTP, action.payload);
    yield put(generateOtpSuccess(user));
  } catch (e) {
    yield put(generateOtpError(e.message));
  }
}
function* ValidateOTP(action: OTPAction) {
  try {
    const user = yield call(JSONPlacholderAPI.validateOtp, action.payload);
    yield put(validateOtpSuccess(user));
  } catch (e) {
    yield put(validateOtpERROR(e.message));
  }
}

export default function* root() {
  yield takeEvery(UsersTypes.FETCH, fetchUser);
  yield takeEvery(CreateUserTypes.CREATE_USER, createUser);
  yield takeEvery(OTPTYPE.VALIDATETEOTP, ValidateOTP);
  yield takeEvery(OTPTYPE.GENERATEOTP, GenerateOTP);
  yield takeEvery(DashboardType.FETCH, fetchDashboard);
}
