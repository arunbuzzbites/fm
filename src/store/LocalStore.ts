import AsyncStorage from '@react-native-community/async-storage';

export class LocalStore {
  static async saveLoginData(loginData: any) {
    try {
      await AsyncStorage.setItem('loginData', JSON.stringify(loginData));
    } catch (err) {
      console.log(err);
    }
  }

  static async getLoginData(): Promise<any | null> {
    return AsyncStorage.getItem('loginData');
  }
}
