import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {View} from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
// import { enableScreens } from 'react-native-screens';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';

import {navigationRef} from './lib/NavigationService';
import {persistor, store} from './store';
import AppNavigator from './AppNavigator';
// enableScreens();

export default class Root extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<View />} persistor={persistor}>
          <NavigationContainer ref={navigationRef}>
            {/*if (userid==='0')*/}
            {/*<Login />*/}
            {/*else*/}
            {/*<LoginNavigator />*/}
            <SafeAreaProvider>
              <AppNavigator />
            </SafeAreaProvider>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}
