import Settings from './containers/Settings';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import Tickets from './containers/Tickets';
import Sale from './containers/Sale';
import Dashboard from './containers/Dashboard';
import {Image} from 'react-native';

const Tab = createBottomTabNavigator();

// export type AppTabParamList = {
//   Home: undefined;
//   Settings: {userID?: string};
// };

const App = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        safeAreaInsets: {bottom: 0, top: 0},
        activeTintColor: '#fff',
        //  inactiveTintColor: 'lightgray',
        // activeBackgroundColor: '#c4461c',
        // inactiveBackgroundColor: '#b55031',
        style: {
          backgroundColor: '#000',
          paddingBottom: 3,
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName = {};

          if (route.name === 'Dashboard') {
            iconName = focused
              ? require('./img/home_white.png')
              : require('./img/home_grey_Copy.png');
          } else if (route.name === 'Account') {
            iconName = focused ? require('./img/setting.png') : require('./img/setting-Copy.png');
          } else if (route.name === 'Sale') {
            iconName = focused ? require('./img/tag.png') : require('./img/tag_Copy.png');
          } else if (route.name === 'Tickets') {
            iconName = focused ? require('./img/gift_white.png') : require('./img/cinema.png');
          }

          // You can return any component that you like here!
          return <Image source={iconName} />;
        },
      })}>
      <Tab.Screen name="Dashboard" component={Dashboard} />
      <Tab.Screen name="Tickets" component={Tickets} />
      <Tab.Screen name="Sale" component={Sale} />
      <Tab.Screen name="Account" component={Settings} />
    </Tab.Navigator>
  );
};

export default App;
