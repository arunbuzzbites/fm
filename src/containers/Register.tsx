import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {navigationRef} from '../lib/NavigationService';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';
import {OPERATIONALENTITYINFO, ADDRESSINFO, RegisterUser} from '../models/ModelFile';
import * as UserActions from '../store/ducks/users/actions';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import normalize from 'react-native-normalize';
import Toast from 'react-native-tiny-toast';
import {RandomNumberGenrator} from '../lib/RandomNumberGenrator';
import {SafeAreaView} from 'react-native-safe-area-context';
interface StateProps {
  user: any;
}

interface DispatchProps {
  createUser(payload: any): void;
}

type Props = StateProps & DispatchProps;

export class Register extends Component<Props> {
  state = {
    userName: '',
    enterPhoneNumber: '',
    password: '',
    cardNumber: '',
    address: '',
    district: '',
    state: '',
    city: '',
    isTermsChecked: false,
    isRegistering: false,
  };

  UNSAFE_componentWillReceiveProps(props: Props): void {
    try {
      if (
        props?.user?.RESPONSEINFO != null &&
        props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed'
      ) {
        this.setState({isRegistering: false});
        Toast.show(
          'Registration failed with error code :' + this.props.user?.RESPONSEINFO?.STATUS.ERRORNO,
        );
      } else if (
        props?.user?.RESPONSEINFO != null &&
        props.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
      ) {
        this.setState({isRegistering: false});
        navigationRef.current?.navigate('VerifyOTP', {
          phoneNumber: this.state.enterPhoneNumber,
        });
      }
    } catch (e) {
      Toast.show(e);
    }
    // console.debug('--- previous Props---' + prevProps);
    //   console.debug('--- previous state---' + prevState);
  }
  validateFields() {
    if (this.state.userName === '') {
      Toast.show('Please Enter Username');
      return false;
    }
    if (this.state.userName.length < 3) {
      Toast.show('Please Enter at least 3 character in userName');
      return false;
    }
    if (this.state.enterPhoneNumber === '') {
      Toast.show('Please Enter a Phone Number');
      return false;
    }
    if (this.state.password === '') {
      Toast.show('Please Enter a Password');
      return false;
    }
    if (this.state.password.length < 5) {
      Toast.show('Password length should b greater than 5');
      return false;
    }
    if (this.state.cardNumber === '') {
      Toast.show('Please Enter Card Number');
      return false;
    }
    if (this.state.password.length < 5) {
      Toast.show('Please enter a valid card number');
      return false;
    }
    if (this.state.address === '') {
      Toast.show('Please Enter your address');
      return false;
    }
    if (this.state.city === '') {
      Toast.show('Please Enter City Name');
      return false;
    }
    if (this.state.district === '') {
      Toast.show('Please Enter Your District Name');
      return false;
    }
    if (this.state.state === '') {
      Toast.show('Please Enter Your State Name');
      return false;
    }
    if (!this.state.isTermsChecked) {
      Toast.show('You must check our Terms & Conditions');
      return false;
    }
    return true;
  }

  registerUser() {
    // navigationRef.current?.navigate('VerifyOTP', {
    //   phoneNumber: '9900109017',
    // });

    if (this.validateFields()) {
      this.setState({isRegistering: true});
      OPERATIONALENTITYINFO.ENTITYTYPE = 'DISTRIBUTO';
      const genratedNumber = RandomNumberGenrator.genrateNumber();
      OPERATIONALENTITYINFO.ENTITYCODE = '' + genratedNumber;
      OPERATIONALENTITYINFO.ENTITYNAME = this.state.userName;
      OPERATIONALENTITYINFO.MOBILEPHONE = this.state.enterPhoneNumber;
      OPERATIONALENTITYINFO.USERNAME = this.state.userName;
      OPERATIONALENTITYINFO.PASSWORD = this.state.password;

      ADDRESSINFO.ADDRESS1 = this.state.address;
      ADDRESSINFO.CITY = this.state.city;
      // ADDRESSINFO.AREA = this.state.district;
      //ADDRESSINFO.ADDRESS2 = this.state.state;

      OPERATIONALENTITYINFO.ADDRESSINFO = ADDRESSINFO;
      RegisterUser.OPERATIONALENTITYINFO = OPERATIONALENTITYINFO;
      console.debug(RegisterUser);
      this.props.createUser(RegisterUser);
    }
  }

  render() {
    return (
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
              <View style={{padding: normalize(8)}}>
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                    <Icon name={'hand-left'} type="material-community" size={14} color="white" />
                    <Text style={{color: 'white', marginStart: 5, fontSize: 10}}>Need Help</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{padding: normalize(8)}}
                  onPress={() => navigationRef.current?.goBack()}>
                  <Icon name={'arrow-left'} type="material-community" size={24} color="white" />
                </TouchableOpacity>
                <Image
                  style={{alignSelf: 'center'}}
                  source={require('../img/dc_yellow_and_blue.png')}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'account'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="Enter Name"
                    onChangeText={(text) => {
                      this.setState({userName: text});
                    }}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.userName}
                  />
                </View>
                <View
                  style={{
                    marginTop: 5,
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'phone'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="Phone Number"
                    keyboardType="numeric"
                    onChangeText={(text) => this.setState({enterPhoneNumber: text})}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.enterPhoneNumber}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: 5,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'lock-outline'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    onChangeText={(text) => this.setState({password: text})}
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.password}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: 5,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'account-box'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="Aadhar Card/ Registration Number"
                    onChangeText={(text) => this.setState({cardNumber: text})}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.cardNumber}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: 5,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="Address"
                    onChangeText={(text) => this.setState({address: text})}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.address}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: 5,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    onChangeText={(text) => this.setState({city: text})}
                    placeholder="City"
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.city}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: 5,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="District"
                    onChangeText={(text) => this.setState({district: text})}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.district}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: 5,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    onChangeText={(text) => this.setState({state: text})}
                    placeholder="State"
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.state}
                  />
                </View>
                <View style={{height: normalize(30)}} />
                <TouchableOpacity
                  disabled={this.state.isRegistering}
                  // onPress={() => navigationRef.current?.navigate('VerifyOTP')}
                  onPress={() => {
                    this.registerUser();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(50),
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: Color.BACKGROUND_COLOR,
                      borderRadius: normalize(50) / 2,
                    }}>
                    <Text
                      accessibilityState={{disabled: this.state.isRegistering}}
                      style={{color: 'white', fontSize: normalize(16), alignSelf: 'center'}}>
                      SIGN UP
                    </Text>
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(30)}} />
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    style={styles.checkBox}
                    onPress={() => {
                      this.setState({isTermsChecked: !this.state.isTermsChecked});
                    }}>
                    <Icon
                      size={normalize(20)}
                      color={'white'}
                      type="material-community"
                      name={
                        this.state.isTermsChecked
                          ? 'checkbox-marked-outline'
                          : 'checkbox-blank-outline'
                      }
                    />
                  </TouchableOpacity>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{color: Color.ICON_COLOR, fontSize: normalize(12)}}>
                      {' '}
                      I agree to the{' '}
                    </Text>
                    <TouchableOpacity>
                      <Text style={{color: 'white', fontWeight: 'bold', fontSize: normalize(12)}}>
                        {' '}
                        Terms & Condition{' '}
                      </Text>
                    </TouchableOpacity>
                    <Text style={{color: Color.ICON_COLOR, fontSize: normalize(12)}}> And </Text>
                    <TouchableOpacity>
                      <Text style={{color: 'white', fontWeight: 'bold', fontSize: normalize(12)}}>
                        {' '}
                        Privacy Policy
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{height: normalize(12)}} />

                <View style={{height: normalize(30)}} />
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  upperPortion: {
    margin: normalize(10),
  },
  bottomPortion: {
    margin: normalize(10),

    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
