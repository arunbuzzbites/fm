import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import normalize from 'react-native-normalize';
import {SafeAreaView} from 'react-native-safe-area-context';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import NavigationService from '../lib/NavigationService';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: string): void;
}

type Props = StateProps & DispatchProps;

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d721',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7211',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7211',
    title: 'Third Item',
  },
];
const Item = ({title}) => (
  <View style={styles.item}>
    <TouchableOpacity
      onPress={() => {
        NavigationService.navigate('ReviewSaleOne', {
          movieName: 'TestMovie',
          movieGenere: 'Action',
          expiryDate: '12-14-2021',
          balance: '150',
        });
      }}>
      <View
        style={{
          marginStart: normalize(8),
          backgroundColor: Color.BACKGROUND_COLOR,
          borderRadius: normalize(5),
          padding: normalize(8),
        }}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex: 1.3}}>
            <View style={{flex: 1}}>
              <Text style={styles.title}>Movie Name</Text>
              <Text style={styles.subHeading}>Expiry Date: 15/11/2020</Text>
            </View>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
              <Text style={{fontSize: normalize(14), color: 'white'}}>Ticket Price: </Text>

              <Text style={{fontSize: normalize(14), color: 'white'}}>120</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
              <Text style={{fontSize: normalize(14), color: 'white'}}>Balance :</Text>
              <Text style={{fontSize: normalize(14), color: 'green', marginEnd: 3}}>120</Text>
              <Text style={{fontSize: normalize(14), color: 'white'}}>Tickets</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  </View>
);
export class Sale extends Component<Props> {
  componentDidMount(): void {
    //   this.props.fetchUser('1');
  }
  renderItem = ({item}) => <Item title={item.title} />;
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
          <View style={{flex: 1}}>
            <FlatList data={DATA} renderItem={this.renderItem} keyExtractor={(item) => item.id} />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sale);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: normalize(5),

    marginVertical: normalize(8),
    marginHorizontal: normalize(8),
  },
  title: {
    fontSize: normalize(20),
    color: 'white',
  },
  subHeading: {
    fontSize: normalize(14),
    color: Color.ICON_COLOR,
  },
});
