import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';
import NavigationService, {navigationRef} from '../lib/NavigationService';
import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import normalize from 'react-native-normalize';
import {AuthenticateUser, AUTHENTICATEUSER} from '../models/ModelFile';
import Toast from 'react-native-tiny-toast';
import {SafeAreaView} from 'react-native-safe-area-context';
import {LocalStore} from '../store/LocalStore';
interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: any): void;
  dashBoard(payload: any): void;
}

type Props = StateProps & DispatchProps;

export class Login extends Component<Props> {
  state = {
    phoneNumber: '',
    password: '',
  };

  UNSAFE_componentWillReceiveProps(props: Props): void {
    if (props.user?.RESPONSEINFO?.STATUS?.MESSAGE === 'Success') {
      // KEY_NAMEVALUE.KEY_NAME = 'PROCESS';
      //  KEY_NAMEVALUE.KEY_VALUE = 'DISTRIBUTORDASHBOARDDTLS';
      //  dashboard.KEY_NAMEVALUE = KEY_NAMEVALUE;
      //  ADDITIONAL_INFO.DISTRIBUTORID = props.user?.RESPONSEINFO?.USERINFO?.REF_ID;
      // dashboard.ADDITIONAL_INFO = ADDITIONAL_INFO;
      //dashboard.ADDITIONAL_INFO = `{"DISTRIBUTORID":${value}}`;
      // this.props.dashBoard(dashboard);
      // if (props.user?.RESPONSEINFO?.STATUS?.MESSAGE === 'Success') {
      //   navigationRef.current?.navigate('homeScreen', {
      //     user: props.user?.RESPONSEINFO?.USERINFO?.USER_ID,
      //   });
      // } else {
      //   Toast.show('tesiinng');
      // }

      LocalStore.saveLoginData(props.user?.RESPONSEINFO?.USERINFO);
      //console.debug(props.user?.RESPONSEINFO?.USERINFO?.REF_ID);
      NavigationService.resetAndNavigate('homeScreen', {
        ref_id: props.user?.RESPONSEINFO?.USERINFO?.REF_ID,
      });
    } else if (props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed') {
      Toast.show('Login is failed with error number: ' + props.user.RESPONSEINFO?.STATUS.ERRORNO);
    }
  }

  validateFields() {
    if (this.state.phoneNumber === '') {
      Toast.show('Please Enter Username');
      return false;
    }
    if (this.state.password === '') {
      Toast.show('Please Enter Password');
      return false;
    }

    return true;
  }

  userLogin() {
    // console.log('hello', 123);

    //  NavigationService.resetAndNavigate('homeScreen' );

    if (this.validateFields()) {
      //this.setState({isRegistering: true});
      AUTHENTICATEUSER.USERNAME = '' + this.state.phoneNumber;
      AUTHENTICATEUSER.PASSWORD = '' + this.state.password;
      AuthenticateUser.AUTHENTICATEUSER = AUTHENTICATEUSER;

      this.props.fetchUser(AuthenticateUser);
    }
  }
  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
              <View style={styles.upperPortion}>
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                    <Icon name={'hand-left'} type="material-community" size={14} color="white" />
                    <Text style={{color: 'white', marginStart: 5, fontSize: 10}}>Need Help</Text>
                  </View>
                </TouchableOpacity>
                <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}}>
                  <Image source={require('../img/dc_yellow_and_blue.png')} />
                </View>
              </View>
              <View style={styles.bottomPortion}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'phone'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="Phone Number"
                    onChangeText={(text) => {
                      this.setState({phoneNumber: text});
                    }}
                    value={this.state.phoneNumber}
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(15),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: normalize(10)}}
                    name={'lock-outline'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder="Password"
                    onChangeText={(text) => {
                      this.setState({password: text});
                    }}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.password}
                    secureTextEntry={true}
                  />
                </View>

                <TouchableOpacity
                  style={{marginTop: normalize(10), alignSelf: 'flex-end'}}
                  onPress={() => navigationRef.current?.navigate('ResetPassword')}>
                  <Text style={{color: 'white', fontSize: normalize(16)}}>Forgot Password</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.userLogin();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(50),
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: Color.BACKGROUND_COLOR,
                      borderRadius: normalize(50) / 2,
                      marginTop: normalize(15),
                    }}>
                    <Text style={{color: 'white', textAlign: 'center', fontSize: normalize(16)}}>
                      LOGIN
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity

                    onPress={() => navigationRef.current?.navigate('Register')}>
                  <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(15),
                  }}>

                    <Text style={{color: 'white', fontSize: normalize(16), alignSelf: 'center'}}>
                      SIGN UP
                    </Text>

                </View>
                </TouchableOpacity>
                <View style={{height: 60}} />
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
