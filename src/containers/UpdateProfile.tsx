import React, {Component} from 'react';
import {
  Image,
  ImageBackground,

  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import normalize from 'react-native-normalize';

import {navigationRef} from '../lib/NavigationService';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: string): void;
}

type Props = StateProps & DispatchProps;

export class UpdateProfile extends Component<Props> {
  componentDidMount(): void {
    //   this.props.fetchUser('1');
  }

  render() {
    return (
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
              <View style={{padding: normalize(8)}}>
                <View style={{height: normalize(30)}} />
                <View
                  style={{
                    width: normalize(100),
                    height: normalize(100),
                    alignSelf: 'center',
                  }}>
                  <Image
                    style={{width: normalize(100), height: normalize(100)}}
                    source={require('../img/user_placeholder.png')}
                  />
                  <Image
                    style={{position: 'absolute', bottom: 0, alignSelf: 'flex-end'}}
                    source={require('../img/images.png')}
                  />
                </View>
                <View style={{height: normalize(30)}} />
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'account'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="Enter Name"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    marginTop: normalize(10),
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'phone'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="Phone Number"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(10),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'lock-outline'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(10),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'account-box'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="Aadhar Card/ Registration Number"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(10),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="Address"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(10),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="City"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(10),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="District"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                    marginTop: normalize(10),
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'map-marker'}
                    type="material-community"
                    size={normalize(20)}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="State"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View style={{height: normalize(50)}} />
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                    alignContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(50) / 2,
                  }}>
                  <TouchableOpacity style={{flex: 1}}>
                    <Text style={{color: 'white', fontSize: 16, alignSelf: 'center'}}>Update</Text>
                  </TouchableOpacity>
                </View>

                <View style={{height: normalize(30)}} />
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  upperPortion: {
    margin: 10,
  },
  bottomPortion: {
    margin: 10,

    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
