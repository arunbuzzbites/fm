import React, {Component} from 'react';
import {ImageBackground, StyleSheet, Text, View, FlatList, Image} from 'react-native';
import normalize from 'react-native-normalize';
import {SafeAreaView} from 'react-native-safe-area-context';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import {dashboard} from '../models/ModelFile';
import Toast from 'react-native-tiny-toast';
import {LocalStore} from '../store/LocalStore';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchDashboard(payload: any): void;
}

type Props = StateProps & DispatchProps;

const Item = ({mobileNumber, movieName, soldQty, orderId, date}) => (
  <View style={styles.item}>
    <View
      style={{
        marginStart: normalize(8),
        backgroundColor: Color.BACKGROUND_COLOR,
        borderRadius: normalize(5),
        padding: normalize(8),
      }}>
      <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
        <Text style={styles.title}>{mobileNumber}</Text>
        <Image style={{margin: normalize(5)}} source={require('../img/ic_remove.png')} />
      </View>

      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(14), color: 'white'}}>{movieName}</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(12), color: 'white'}}>Tickets Quantity :</Text>
            <Text style={{fontSize: normalize(12), color: 'white'}}>{' ' + soldQty}</Text>
          </View>
        </View>
        <View>
          <View
            style={{
              flex: 1,

              position: 'absolute',
              bottom: 0,
              alignSelf: 'flex-end',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
              <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>Sale Date :</Text>
              <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>{date}</Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
              <Text style={{fontSize: normalize(12), color: 'white'}}>Order Id :</Text>
              <Text style={{fontSize: normalize(12), color: 'white'}}>{orderId}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  </View>
);

export class SaleHistory extends Component<Props> {
  state = {
    data: [
      //   {
      // MOVIE: 'MZPE',
      // CUSTOMERMOBILENUMBER: '99857533841',
      // SOLDQUANTITY: '2',
      // TOTALPRICE: '',
      // ORDER_ID: '66',
      // SALE_DATE: '16/11/2020',
      // },
    ],
    shouldRefresh: false,
  };

  componentDidMount(): void {
    console.debug(this.props);
    try {
      this.getValueFromStore();
    } catch (e) {
      console.debug(e);
    }
  }
  getValueFromStore() {
    const userData = LocalStore.getLoginData();
    userData
      .then((userData) => JSON.parse(userData))
      .then((userData) => {
        console.debug('From local store ' + userData);
        if (userData == null) {
          dashboard.KEY_NAMEVALUE = {KEY_NAME: 'PROCESS', KEY_VALUE: 'DISTRIBUTORSOLDLIST'};
          dashboard.ADDITIONAL_INFO = "{DISTRIBUTORID: '250'}";

          console.debug(dashboard);
          this.props.fetchDashboard(dashboard);
        } else {
          dashboard.KEY_NAMEVALUE = {KEY_NAME: 'PROCESS', KEY_VALUE: 'DISTRIBUTORSOLDLIST'};
          const refid = userData?.REF_ID;
          dashboard.ADDITIONAL_INFO = "{DISTRIBUTORID: '" + refid + "'}";
          //     dashboard.ADDITIONAL_INFO = "{DISTRIBUTORID: '994'}";
          console.debug(refid);
          console.debug(userData);
          this.props.fetchDashboard(dashboard);
        }
      });
  }

  UNSAFE_componentWillReceiveProps(props): void {
    if (props.user?.RESPONSEINFO?.STATUS?.MESSAGE === 'Success') {
      if (
        props.user?.RESPONSEINFO != null &&
        props.user?.RESPONSEINFO?.DISTRIBUTORSOLDLIST != null
        // props.user?.RESPONSEINFO?.DISTRIBUTORSOLDLIST.length > 0
      ) {
        this.setState({
          data: this.state.data.concat(props.user?.RESPONSEINFO?.DISTRIBUTORSOLDLIST),
          shouldRefresh: !this.state.shouldRefresh,
        });
      }
    } else if (props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed') {
      Toast.show('Api failed with error number: ' + props.user.RESPONSEINFO?.STATUS.ERRORNO);
    }

    // if (this.props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed') {
    //   Toast.show(
    //       'OTP Generation Error. Error code is :' + this.props.user.RESPONSEINFO?.STATUS.ERRORNO,
    //   );
    // } else if (
    //     this.state.resendPasswordApi &&
    //     this.props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
    // ) {
    //   this.setState({resendPasswordApi: false});
    //   Toast.show('OTP Sent on  :' + this.phonenumber);
    // } else if (
    //     this.state.isValidateApi &&
    //     this.props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
    // ) {
    //   NavigationService.resetAndNavigate('homeScreen');
    // }
  }

  renderItem = ({item}) => (
    <Item
      mobileNumber={item.CUSTOMERMOBILENUMBER}
      movieName={item.MOVIE}
      soldQty={item.SOLDQUANTITY}
      orderId={item.ORDER_ID}
      date={item.SALE_DATE}
    />
  );
  EmptyListMessage = () => {
    return (this.state.data==null || this.state.data.length == 0) ? <Text style={styles.emptyListStyle}>No Data Found</Text> : null;
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
          <View style={{flex: 1}}>
            {this.state.data != null && this.state.data.length > 0 ? (
              <Text
              style={{
                color: 'white',
                  textAlign: 'center',
                width: normalize(70),
                fontSize: normalize(12),
                backgroundColor: Color.BACKGROUND_COLOR,
                margin: normalize(8),
                padding: normalize(5),
                borderRadius: normalize(20),
                alignSelf: 'flex-end',
              }}>
              {' '}
              Clear All
            </Text>) : null}
            <FlatList
              data={this.state.data}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.ORDER_ID}
              extraData={this.state.shouldRefresh}
              ListEmptyComponent={this.EmptyListMessage()}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SaleHistory);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  emptyListStyle: {
    padding: 10,
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(20),
    color: Color.APP_YELLOW,
  },
  subHeading: {
    fontSize: normalize(12),
    color: Color.ICON_COLOR,
  },
});
