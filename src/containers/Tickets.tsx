import React, {Component} from 'react';
import {ImageBackground, StyleSheet, Text, View, FlatList} from 'react-native';
import normalize from 'react-native-normalize';
import {SafeAreaView} from 'react-native-safe-area-context';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: string): void;
}

type Props = StateProps & DispatchProps;

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d721',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7211',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7211',
    title: 'Third Item',
  },
];
const Item = ({title}) => (
  <View style={styles.item}>
    <View
      style={{
        marginStart: normalize(8),
        backgroundColor: Color.BACKGROUND_COLOR,
        borderRadius: normalize(5),
        padding: normalize(8),
      }}>
      <Text style={styles.title}>Movie Name</Text>
      <Text style={styles.subHeading}>Expiry Date: 15/11/2020</Text>
    </View>
  </View>
);
export class Tickets extends Component<Props> {
  componentDidMount(): void {
    //   this.props.fetchUser('1');
  }
  renderItem = ({item}) => <Item title={item.title} />;
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
          <View style={{flex: 1, margin: normalize(5) , alignSelf : 'center'}}>
            <Text
              style={{
                color: 'white',
                alignSelf: 'center',
                fontWeight: 'bold',
                fontSize: normalize(20),
                margin: normalize(8),
              }}>
              BUY MOVIES
            </Text>
            <FlatList data={DATA}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.id}
              numColumns={2}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  upperPortion: {
    margin: normalize(10),
  },
  bottomPortion: {
    margin: normalize(10),

    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: normalize(10),
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: normalize(5),

    marginVertical: normalize(8),
    marginHorizontal: normalize(8),
  },
  title: {
    fontSize: normalize(18),
    color: 'white',
  },
  subHeading: {
    fontSize: normalize(12),
    color: Color.ICON_COLOR,
  },
});
