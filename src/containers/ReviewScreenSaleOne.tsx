import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';
import NavigationService, {navigationRef} from '../lib/NavigationService';
import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import normalize from 'react-native-normalize';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Picker} from '@react-native-picker/picker';
interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: any): void;
}

type Props = StateProps & DispatchProps;

export class ReviewScreenSaleOne extends Component<Props> {
  state = {
    movieName: 'Movie Name',
    movieGenere: '(Action)',
    expiry: '12/15/2020',
    saleQuantity: '150',
    phoneNumber: '9900109017',
    selectedQuantity: '5',
  };
  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/asset-12.png')} style={styles.image}>
              <View style={{flex: 1}}>
                <View style={styles.upperPortion}>
                  <TouchableOpacity
                    onPress={() => {
                      NavigationService.goBack();
                    }}>
                    <View
                      style={{flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-start'}}>
                      <Icon
                        name={'chevron-back'}
                        type="ionicon"
                        size={normalize(26)}
                        color="white"
                      />
                      <Text
                        style={{
                          color: 'white',
                          marginStart: normalize(5),
                          fontSize: normalize(16),
                        }}>
                        {this.state.movieName}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}} />
                </View>
                <View style={styles.bottomPortion}>
                  <Text style={{fontSize: normalize(18), color: 'white', alignSelf: 'center'}}>
                    {this.state.movieName}
                  </Text>
                  <Text style={{fontSize: normalize(14), color: 'white', alignSelf: 'center'}}>
                    {this.state.movieGenere}
                  </Text>
                  <View style={{height: normalize(20)}} />

                  <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                    <Text style={{fontSize: normalize(14), color: 'white'}}>Expiry Date:</Text>
                    <View style={{width: normalize(8)}} />
                    <Text style={{fontSize: normalize(14), color: 'white', alignSelf: 'center'}}>
                      {this.state.expiry}
                    </Text>
                  </View>
                  <View style={{height: normalize(10)}} />
                  <Text
                    style={{fontSize: normalize(14), color: Color.ICON_COLOR, alignSelf: 'center'}}>
                    {' '}
                    {'You can sale ' + this.state.saleQuantity + ' tickets in a single transition.'}
                  </Text>

                  <View style={{height: normalize(30)}} />
                  <View
                    style={{
                      backgroundColor: Color.BACKGROUND_COLOR,
                      borderRadius: normalize(5),
                      padding: normalize(16),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: normalize(40),
                        backgroundColor: Color.ICON_COLOR,
                        borderRadius: normalize(40) / 2,
                      }}>
                      <Icon
                        style={{alignSelf: 'center', margin: normalize(10)}}
                        name={'phone'}
                        type="material-community"
                        size={normalize(20)}
                        color={Color.BACKGROUND_COLOR}
                      />
                      <TextInput
                        style={{flex: 1, color: Color.BACKGROUND_COLOR}}
                        placeholder="Phone Number"
                        onChangeText={(text) => {
                          this.setState({phoneNumber: text});
                        }}
                        value={this.state.phoneNumber}
                        placeholderTextColor={Color.BACKGROUND_COLOR}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: normalize(40),
                        backgroundColor: Color.ICON_COLOR,
                        borderRadius: normalize(40) / 2,
                        marginTop: normalize(15),
                      }}>
                      <Icon
                        style={{alignSelf: 'center', margin: normalize(10)}}
                        name={'ios-pricetags'}
                        type="ionicon"
                        size={normalize(20)}
                        color={Color.BACKGROUND_COLOR}
                      />
                      <Picker
                        selectedValue={this.state.selectedQuantity}
                        style={{height: normalize(40), flex: 1}}
                        mode={'dropdown'}
                        onValueChange={(itemValue, itemIndex) =>
                          this.setState({selectedQuantity: itemValue})
                        }>
                        {Array.from(Array(Number(this.state.saleQuantity)).keys()).map((item) => (
                          <Picker.Item key={item + 1} label={`${item + 1}`} value={item + 1} />
                        ))}
                      </Picker>
                    </View>
                  </View>
                </View>

                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    bottom: 0,
                    width: '100%',
                  }}
                  onPress={() => {
                    NavigationService.navigate('ReviewSaleTwo');
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',

                      height: normalize(45),
                      justifyContent: 'center',
                      backgroundColor: Color.BACKGROUND_COLOR,
                    }}>
                    <Text style={{color: 'white', fontSize: normalize(16), alignSelf: 'center'}}>
                      Review Sale
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReviewScreenSaleOne);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
