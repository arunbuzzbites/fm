import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';
import NavigationService, {navigationRef} from '../lib/NavigationService';
import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import normalize from 'react-native-normalize';
import {AuthenticateUser, AUTHENTICATEUSER} from '../models/ModelFile';
import Toast from 'react-native-tiny-toast';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Picker} from '@react-native-picker/picker';
interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: any): void;
}

type Props = StateProps & DispatchProps;

export class ReviewScreenSaleTwo extends Component<Props> {
  state = {
    movieName: 'Movie Name',
    movieGenere: '(Action)',
    expiry: '12/15/2020',
    saleQuantity: '150',
    phoneNumber: '9900109017',
    selectedQuantity: '5',
  };
  createTable = () => {
    const items = [];

    // Outer loop to create parent
    for (let i = 0; i < Number(this.state.saleQuantity); i++) {
      items.push(<Picker.Item label={i.toString()} value={i.toString()} />);
    }
    //Create the parent and add the children

    console.debug(items);
    return items;
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/asset-12.png')} style={styles.image}>
              <View style={{flex: 1}}>
                <View style={styles.upperPortion}>
                  <TouchableOpacity
                    onPress={() => {
                      NavigationService.goBack();
                    }}>
                    <View
                      style={{flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-start'}}>
                      <Icon
                        name={'chevron-back'}
                        type="ionicon"
                        size={normalize(26)}
                        color="white"
                      />
                      <Text
                        style={{
                          color: 'white',
                          marginStart: normalize(5),
                          fontSize: normalize(16),
                        }}>
                        Review Sale
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}} />
                </View>
                <View style={styles.bottomPortion}>
                  <View style={styles.item}>
                    <View
                      style={{
                        marginStart: normalize(8),
                        backgroundColor: Color.BACKGROUND_COLOR,
                        borderRadius: normalize(5),
                        padding: normalize(8),
                      }}>
                      <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
                        <Text style={styles.title}>+91 234567890</Text>
                        <Image
                          style={{margin: normalize(5)}}
                          source={require('../img/ic_remove.png')}
                        />
                      </View>
                      <Text style={styles.subHeading}>Movie Name</Text>

                      <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 1}}>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                            <Text style={{fontSize: normalize(12), color: 'white'}}>
                              Tickets Quantity :
                            </Text>
                            <Text style={{fontSize: normalize(12), color: 'white'}}>1</Text>
                          </View>
                        </View>
                        <View>
                          <View
                            style={{
                              flex: 1,

                              position: 'absolute',
                              bottom: 0,
                              alignSelf: 'flex-end',
                            }}>
                            {/*<View*/}
                            {/*  style={{*/}
                            {/*    flexDirection: 'row',*/}
                            {/*    justifyContent: 'flex-end',*/}
                            {/*  }}>*/}
                            {/*  <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>*/}
                            {/*    Sale Date :*/}
                            {/*  </Text>*/}
                            {/*  <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>*/}
                            {/*    15/11/2020*/}
                            {/*  </Text>*/}
                            {/*</View>*/}

                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'flex-end',
                              }}>
                              <Text style={{fontSize: normalize(14), color: 'white'}}>
                                Total Price :
                              </Text>
                              <View style={{width: normalize(5)}} />
                              <Text style={{fontSize: normalize(12), color: 'white'}}>₹ 160</Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>

                <TouchableOpacity
                  style={{
                    position: 'absolute',
                    bottom: 0,
                    width: '100%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(45),
                      justifyContent: 'center',
                      backgroundColor: 'lightgreen',
                    }}>
                    <Text
                      style={{
                        marginStart: normalize(5),
                        color: 'white',
                        fontSize: normalize(16),
                        flex: 1,
                        alignSelf: 'center',
                      }}>
                      Total Tickets :
                    </Text>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(24),
                        flex: 1,
                        alignSelf: 'center',
                      }}>
                      6
                    </Text>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(16),
                        flex: 1,
                        alignSelf: 'center',
                      }}>
                      Total Price
                    </Text>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(24),
                        flex: 1,
                        alignSelf: 'center',
                      }}>
                      ₹ 160
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReviewScreenSaleTwo);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(20),
    color: Color.APP_YELLOW,
  },
  subHeading: {
    fontSize: normalize(14),
    color: 'white',
  },
});
