import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import normalize from 'react-native-normalize';
import Icon from 'react-native-easy-icon';
import NavigationService, {navigationRef} from '../lib/NavigationService';

export default class Settings extends React.PureComponent {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
            <ScrollView style={styles.container}>
              <View style={styles.bottomPortion}>
                <TouchableOpacity
                  onPress={() => {
                    navigationRef.current?.navigate('OrderHistory');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/order_history.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Order History
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />

                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    navigationRef.current?.navigate('ReviewSaleOne');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/order_history.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      ReviewScreenSale
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />

                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    navigationRef.current?.navigate('ReviewSaleTwo');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/order_history.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      ReviewScreenSale2
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />

                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    navigationRef.current?.navigate('SaleHistory');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Icon
                      name={'share-square'}
                      type="font-awesome5"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Sale History
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    navigationRef.current?.navigate('UpdateProfile');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/account.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      My Account
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/key.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Reset Password
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/terms.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Terms & Conditions
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/privacy.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Privacy Policy
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Icon
                      name={'email-outline'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Contact Us
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../img/information.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Help
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    NavigationService.resetAndNavigate('Login');
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name={'logout'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      Sign out
                    </Text>

                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View style={{height: 1, backgroundColor: 'lightslategrey', width: '100%'}} />
              </View>
            </ScrollView>
          </ImageBackground>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  bottomPortion: {
    padding: normalize(10),
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
  },
});
