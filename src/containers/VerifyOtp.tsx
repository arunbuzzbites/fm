import React, {Component} from 'react';
import {
  Image,
  ImageBackground,

  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import NavigationService from '../lib/NavigationService';
import {GENERATEOTP, RegisterOtp, ReValidateOtp, VALIDATEOTP} from '../models/ModelFile';
import normalize from 'react-native-normalize';
import Toast from 'react-native-tiny-toast';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  validateOtp(payload: any): void;
  generateOTP(payload: any): void;
}

type Props = StateProps & DispatchProps;

export class VerifyOtp extends Component<Props> {
  state = {
    firstDigit: '',
    secondDigit: '',
    forthDigit: '',
    thirdDigit: '',
    enterPhoneNumber: '',
    timer: 30,
    isTimeRunning: false,
    isValidateApi: false,
    resendPasswordApi: false,
  };

  phonenumber = '';

  constructor(props) {
    super(props);
    this.phonenumber = props.route.params.phoneNumber;
  }

  UNSAFE_componentDidMount(): void {
    console.debug(this.props);
    try {
      this.setState({enterPhoneNumber: this.phonenumber});
      GENERATEOTP.MOBILEPHONE = this.phonenumber;
      GENERATEOTP.RESEND = 'FALSE';
      RegisterOtp.GENERATEOTP = GENERATEOTP;

      this.props.generateOTP(RegisterOtp);
    } catch (e) {
      console.debug(e);
    }
  }
  UNSAFE_componentWillReceiveProps(props: Props): void {
    try {
      if (
        props?.user?.RESPONSEINFO != null &&
        props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed'
      ) {
        Toast.show(
          'OTP Generation Error. Error code is :' + this.props?.user?.RESPONSEINFO?.STATUS.ERRORNO,
        );
      } else if (
        this.state.resendPasswordApi &&
        props?.user?.RESPONSEINFO != null &&
        props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
      ) {
        this.setState({resendPasswordApi: false});
        Toast.show('OTP Sent on  :' + this.phonenumber);
      } else if (
        props?.user?.RESPONSEINFO != null &&
        this.state.isValidateApi &&
        props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
      ) {
        this.setState({isValidateApi: false});
        NavigationService.resetAndNavigate('homeScreen');
      }
    } catch (e) {
      Toast.show(e);
    }
  }

  validateOtpApi() {
    VALIDATEOTP.MOBILEPHONE = this.phonenumber;
    VALIDATEOTP.OTP =
      this.state.firstDigit +
      '' +
      this.state.secondDigit +
      '' +
      this.state.thirdDigit +
      '' +
      this.state.forthDigit;

    ReValidateOtp.VALIDATEOTP = VALIDATEOTP;
    console.debug(this.props);
    console.debug(ReValidateOtp);
    this.setState({isValidateApi: true});
    this.props.validateOtp(ReValidateOtp);
  }
  // componentWillReceiveProps(props: Props) {
  //   console.debug(props.user);
  //   if (props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed') {
  //     Toast.show('OTP Generation Error. Error code is :' + props.user.RESPONSEINFO?.STATUS.ERRORNO);
  //   }
  // }
  resendCode() {
    this.setState({resendPasswordApi: true});
    GENERATEOTP.MOBILEPHONE = this.phonenumber;
    GENERATEOTP.RESEND = 'TRUE';
    RegisterOtp.GENERATEOTP = GENERATEOTP;
    this.props.generateOTP(RegisterOtp);
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
              <View style={styles.upperPortion}>
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                    <Icon name={'hand-left'} type="material-community" size={14} color="white" />
                    <Text style={{color: 'white', marginStart: 5, fontSize: 10}}>Need Help</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={{padding: 8}} onPress={() => NavigationService.goBack()}>
                  <Icon name={'arrow-left'} type="material-community" size={24} color="white" />
                </TouchableOpacity>
                <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}}>
                  <Image source={require('../img/dc_yellow_and_blue.png')} />
                </View>
                <Text style={{color: Color.ICON_COLOR, alignSelf: 'center'}}>Enter OTP</Text>
              </View>
              <View style={styles.bottomPortion}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(50),
                  }}>
                  <TextInput
                    style={{
                      width: normalize(50),
                      textAlign: 'center',
                      fontSize: normalize(24),
                      backgroundColor: Color.BACKGROUND_COLOR,
                      color: 'white',
                    }}
                    keyboardType={'numeric'}
                    maxLength={1}
                    value={this.state.firstDigit}
                    onChangeText={(text) => {
                      this.setState({firstDigit: text});
                    }}
                  />
                  <View style={{width: normalize(10)}} />
                  <TextInput
                    style={{
                      width: normalize(50),
                      textAlign: 'center',
                      fontSize: normalize(24),
                      backgroundColor: Color.BACKGROUND_COLOR,
                      color: 'white',
                    }}
                    keyboardType={'numeric'}
                    maxLength={1}
                    value={this.state.secondDigit}
                    onChangeText={(text) => {
                      this.setState({secondDigit: text});
                    }}
                  />
                  <View style={{width: normalize(10)}} />
                  <TextInput
                    style={{
                      width: normalize(50),
                      textAlign: 'center',
                      fontSize: normalize(24),
                      backgroundColor: Color.BACKGROUND_COLOR,
                      color: 'white',
                    }}
                    keyboardType={'numeric'}
                    maxLength={1}
                    value={this.state.thirdDigit}
                    onChangeText={(text) => {
                      this.setState({thirdDigit: text});
                    }}
                  />
                  <View style={{width: normalize(10)}} />
                  <TextInput
                    style={{
                      width: normalize(50),
                      textAlign: 'center',
                      fontSize: normalize(24),
                      backgroundColor: Color.BACKGROUND_COLOR,
                      color: 'white',
                    }}
                    keyboardType={'numeric'}
                    maxLength={1}
                    value={this.state.forthDigit}
                    onChangeText={(text) => {
                      this.setState({forthDigit: text});
                    }}
                  />
                </View>
                <Text style={{color: 'white', fontSize: normalize(18), marginTop: normalize(10)}}>
                  Resend OTP in {this.state.timer} sec.
                </Text>

                <Text style={{color: Color.ICON_COLOR, marginTop: normalize(10)}}>
                  OTP sent to your Mobile Number
                </Text>

                <Text style={{color: 'white', fontWeight: 'bold', fontSize: normalize(18)}}>
                  {this.state.enterPhoneNumber}
                </Text>
                <View style={{height: normalize(60)}} />
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    alignContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                    marginTop: 15,
                  }}>
                  <TouchableOpacity
                    style={{flex: 1}}
                    onPress={() => {
                      this.validateOtpApi();
                    }}>
                    <Text style={{color: 'white', fontSize: 16, alignSelf: 'center'}}>CONFIRM</Text>
                  </TouchableOpacity>
                </View>
                <View style={{width: normalize(10)}} />
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    alignContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                    marginTop: 15,
                  }}>
                  <TouchableOpacity
                    disabled={this.state.isTimeRunning}
                    style={{flex: 1}}
                    onPress={() => {
                      this.resendCode();
                    }}>
                    <Text
                      style={{color: 'white', fontSize: 16, alignSelf: 'center'}}
                      accessibilityState={{disabled: this.state.isTimeRunning}}>
                      RESEND
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{height: 60}} />
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(VerifyOtp);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  upperPortion: {
    margin: 10,
    flex: 1,
  },
  bottomPortion: {
    margin: 10,
    flex: 2,

    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
