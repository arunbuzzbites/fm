import React, {Component} from 'react';
import {ImageBackground, StyleSheet, Text, View, FlatList, Image} from 'react-native';
import normalize from 'react-native-normalize';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import {SafeAreaView} from 'react-native-safe-area-context';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: string): void;
}

type Props = StateProps & DispatchProps;

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d721',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7211',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7211',
    title: 'Third Item',
  },
];
const Item = ({title}) => (
  <View style={styles.item}>
    <View
      style={{
        marginStart: normalize(8),
        backgroundColor: Color.BACKGROUND_COLOR,
        borderRadius: normalize(5),
        padding: normalize(8),
      }}>
      <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
        <Text style={styles.title}>Movie Name</Text>
        <Image style={{margin: normalize(5)}} source={require('../img/ic_remove.png')} />
      </View>
      <Text style={styles.subHeading}>(Malayalam Movie)</Text>

      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{flex: 1.3}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(14), color: 'white'}}>Purchased : 250 Tickets,</Text>
            <Text style={{fontSize: normalize(14), marginStart: normalize(5), color: 'white'}}>
              2 Posters
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>Purchased Date :</Text>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>15/11/2020</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>Expiry Date :</Text>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>15/11/2020</Text>
          </View>
        </View>
        <View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              position: 'absolute',
              bottom: 0,
              alignSelf: 'flex-end',
            }}>
            <Text style={{fontSize: normalize(12), color: 'white'}}>Order Id :</Text>
            <Text style={{fontSize: normalize(12), color: 'white', marginEnd: 3}}>01234567</Text>
          </View>
        </View>
      </View>
    </View>
  </View>
);

export class OrderHistory extends Component<Props> {
  componentDidMount(): void {
    //   this.props.fetchUser('1');
  }

  renderItem = ({item}) => <Item title={item.title} />;

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
          <View style={{flex: 1}}>
            <Text
              style={{
                color: 'white',
                textAlign: 'center',
                width: normalize(70),
                fontSize: normalize(12),
                backgroundColor: Color.BACKGROUND_COLOR,
                margin: normalize(8),
                padding: normalize(5),
                borderRadius: normalize(20),
                alignSelf: 'flex-end',
              }}>
              {' '}
              Clear All
            </Text>
            <FlatList data={DATA} renderItem={this.renderItem} keyExtractor={(item) => item.id} />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistory);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(20),
    color: Color.APP_YELLOW,
  },
  subHeading: {
    fontSize: normalize(12),
    color: Color.ICON_COLOR,
  },
});
