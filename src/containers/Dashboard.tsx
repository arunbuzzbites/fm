import React, {Component} from 'react';
import {FlatList, ImageBackground, StyleSheet, Text, View} from 'react-native';
import normalize from 'react-native-normalize';
import {SafeAreaView} from 'react-native-safe-area-context';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';

import {dashboard} from '../models/ModelFile';
import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchDashboard(payload: any): void;
}

type Props = StateProps & DispatchProps;

const Item = ({MOVIE, PURCHASEDQUANTITY, SOLDQUANTITY, AVAILABLEQUANTITY, pur_date}) => (
  <View style={styles.item}>
    <View
      style={{
        marginStart: normalize(8),
        backgroundColor: Color.BACKGROUND_COLOR,
        borderRadius: normalize(5),
        padding: normalize(8),
      }}>
      <Text style={styles.title}>{MOVIE}</Text>
      <Text style={styles.subHeading}>{'(' + MOVIE + ')'}</Text>

      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{flex: 1.3}}>
          <Text style={{fontSize: normalize(14), color: 'white'}}>
            {'Purchased : ' + PURCHASEDQUANTITY + ' Tickets'}
          </Text>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>Purchased Date :</Text>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>{' ' + pur_date}</Text>
          </View>
        </View>
        <View style={{flex: 1}}>
          <View style={{flex: 1, flexDirection: 'row', alignSelf: 'flex-end'}}>
            <Text style={{fontSize: normalize(14), color: 'white'}}>Sold :</Text>
            <Text style={{fontSize: normalize(14), color: 'red', marginStart: 3, marginEnd: 3}}>
              {' ' + SOLDQUANTITY}
            </Text>
            <Text style={{fontSize: normalize(14), color: 'white'}}>Tickets</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row', alignSelf: 'flex-end'}}>
            <Text style={{fontSize: normalize(14), color: 'white'}}>Balance :</Text>
            <Text style={{fontSize: normalize(14), color: 'green', marginEnd: 3}}>
              {' ' + AVAILABLEQUANTITY + ' '}
            </Text>
            <Text style={{fontSize: normalize(14), color: 'white'}}>Tickets</Text>
          </View>
        </View>
      </View>
    </View>
  </View>
);

export class Dashboard extends Component<Props> {
  state = {
    data: [

    ],
    shouldRefresh: false,
  };
  constructor(props) {
    super(props);
  }
  componentDidMount(): void {
    console.debug(this.props);
    try {
      // KEY_NAMEVALUE.KEY_NAME = 'PROCESS';
      //KEY_NAMEVALUE.KEY_VALUE = 'DISTRIBUTORDASHBOARDDTLS';
      dashboard.KEY_NAMEVALUE = {KEY_NAME: 'PROCESS', KEY_VALUE: 'DISTRIBUTORDASHBOARDDTLS'};
      const refid = this.props.user?.RESPONSEINFO?.USERINFO?.REF_ID;
      dashboard.ADDITIONAL_INFO = "{DISTRIBUTORID: '" + refid + "'}";
      //  dashboard.ADDITIONAL_INFO = "{DISTRIBUTORID: '994'}";
      console.debug(dashboard);
      this.props.fetchDashboard(dashboard);
    } catch (e) {
      console.debug(e);
    }

    //this.props.fetchDashboard();
  }
  componentWillReceiveProps(props): void {
    if (props.user?.RESPONSEINFO?.STATUS?.MESSAGE === 'Success') {
      if(props.user?.RESPONSEINFO!=null && props.user?.RESPONSEINFO?.DISTRIBUTORDASHBOARDDETAILS!=null){
        this.setState({
          data: this.state.data.concat(props.user?.RESPONSEINFO?.DISTRIBUTORDASHBOARDDETAILS),
          shouldRefresh: !this.state.shouldRefresh,
        });
      }
    }

    // if (this.props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Failed') {
    //   Toast.show(
    //       'OTP Generation Error. Error code is :' + this.props.user.RESPONSEINFO?.STATUS.ERRORNO,
    //   );
    // } else if (
    //     this.state.resendPasswordApi &&
    //     this.props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
    // ) {
    //   this.setState({resendPasswordApi: false});
    //   Toast.show('OTP Sent on  :' + this.phonenumber);
    // } else if (
    //     this.state.isValidateApi &&
    //     this.props?.user?.RESPONSEINFO?.STATUS.MESSAGE === 'Success'
    // ) {
    //   NavigationService.resetAndNavigate('homeScreen');
    // }
  }

  renderItem = ({item}) => (
    <Item
      MOVIE={item.MOVIE}
      PURCHASEDQUANTITY={item.PURCHASEDQUANTITY}
      SOLDQUANTITY={item.SOLDQUANTITY}
      AVAILABLEQUANTITY={item.AVAILABLEQUANTITY}
      pur_date={item.pur_date}
    />
  );
  EmptyListMessage = () => {
    return (this.state.data==null || this.state.data.length == 0) ? <Text style={styles.emptyListStyle}>No Data Found</Text> : null;
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
          <View style={{flex: 1}}>
            <Text
              style={{
                color: 'white',
                alignSelf: 'center',
                fontWeight: 'bold',
                fontSize: normalize(20),
                margin: normalize(8),
              }}>
              DASHBOARD
            </Text>
            <FlatList
              extraData={this.state.shouldRefresh}
              data={this.state.data}
              renderItem={this.renderItem}
              ListEmptyComponent={this.EmptyListMessage()}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerText: {
    padding: normalize(10),
    height: normalize(40),
  },
  emptyListStyle: {
    padding: 10,
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    fontSize: normalize(20),
    color: 'white',
  },
  subHeading: {
    fontSize: normalize(14),
    color: Color.ICON_COLOR,
  },
});
