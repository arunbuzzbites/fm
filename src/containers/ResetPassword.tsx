import React, {Component} from 'react';
import {
  Image,
  ImageBackground,

  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import Icon from 'react-native-easy-icon';

import * as UserActions from '../store/ducks/users/actions';
import {User} from '../store/ducks/users/types';
import {ApplicationState} from 'src/store';
import {Color} from '../constants/Color';
import {navigationRef} from '../lib/NavigationService';

interface StateProps {
  user: User | null;
}

interface DispatchProps {
  fetchUser(payload: string): void;
}

type Props = StateProps & DispatchProps;

export class ResetPassword extends Component<Props> {
  componentDidMount(): void {
    // this.props.fetchUser('1');
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground source={require('../img/login_bg.png')} style={styles.image}>
              <View style={styles.upperPortion}>
                <TouchableOpacity>
                  <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                    <Icon name={'hand-left'} type="material-community" size={14} color="white" />
                    <Text style={{color: 'white', marginStart: 5, fontSize: 10}}>Need Help</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{padding: 8}}
                  onPress={() => navigationRef.current?.goBack()}>
                  <Icon name={'arrow-left'} type="material-community" size={24} color="white" />
                </TouchableOpacity>
                <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}}>
                  <Image source={require('../img/dc_yellow_and_blue.png')} />
                </View>

                <Text style={{color: 'white', alignSelf: 'center'}}>Reset Password</Text>
              </View>
              <View style={styles.bottomPortion}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'lock-outline'}
                    type="material-community"
                    size={20}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="New Password"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                    marginTop: 15,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'lock-outline'}
                    type="material-community"
                    size={20}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1}}
                    placeholder="Re-Enter New Password"
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>

                <View style={{height: 60}} />

                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    alignContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                  }}>
                  <TouchableOpacity style={{flex: 1}}>
                    <Text style={{color: 'white', fontSize: 16, alignSelf: 'center'}}>CONFIRM</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  user: state.users.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },
  upperPortion: {
    margin: 10,
    flex: 1,
  },
  bottomPortion: {
    margin: 10,
    flex: 2,

    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
